# SEIntro-Webapp
Requirement: 
    Apache Maven.
    Node JS (v14.15.0)

Run:
    - BE:
        + mvn clean install spring-boot:run .
    - FE:
        + npm start
Default account:
    - usrname: manhtruong
    - pass: manhtruong